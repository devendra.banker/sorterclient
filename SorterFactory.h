#ifndef SORTERFACTORY_H
#define SORTERFACTORY_H

#include "ISorter.h"

namespace sorterspace{
class SorterFactory
{
public:
    ISorter *CreateNew(int Type);
};
}
#endif // SORTERFACTORY_H
