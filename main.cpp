#include <iostream>
#include <chrono>
#include "SorterFactory.h"

using namespace std;
using namespace sorterspace;

int main()
{
    int inputData[SORTER_SIZE];
    for (int i = 0; i < SORTER_SIZE; i++) {
        inputData[i] = rand() % SORTER_SIZE;
    }

    SorterFactory sf;
    ISorter *s=sf.CreateNew(SORTER_ALPHA_VERSION);

    int errorCode = SORTER_INVALID;
    int *outputData;
    auto t1 = std::chrono::high_resolution_clock::now();
    outputData = s->sort(inputData, SORTER_SIZE, errorCode);
    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
    if(errorCode == SORTER_SUCCESS)
    {
        cout << "SORTER_ALPHA_VERSION = " << SORTER_ALPHA_VERSION << " "
             << "Time to Sort <" <<SORTER_SIZE <<">" << " Integer Elements is = " << duration << " milliseconds "
             << endl;
    }
    else
    {
        cout << "SORTER_ALPHA_VERSION = " << SORTER_ALPHA_VERSION << " "
             << "Error in Sorting"
             << endl;
    }

    for(int i=0; i < SORTER_SIZE ;  i++ )
    {
        cout << "Sorter Integer Array[" << i << "] = " << outputData[i] << endl;
    }
    return 0;
}
