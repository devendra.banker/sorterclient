#ifndef ISORTER_H
#define ISORTER_H

#include "Sorter_global.h"

namespace sorterspace{
class ISorter
{
public:
    virtual int *sort(int arr[], int size, int &errorCode) = 0;
};
}
#endif // ISORTER_H
