Application to use shared sorter library - libsorter.so

Instructions
------------
1. Copy the library API/Header files in the main folder
2. Link sorter library so manually in CMake

Video Demonstration 
-------------------
  - 5fe58b1fb7a37a117d2db67db98cbf682545c8c1 Commit : Software Trail Youtube Channel : https://www.youtube.com/watch?v=_es7ITLHGKU&t=18s
